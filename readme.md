# Online chat
I have created this app together with my colleague, who wrote simple server in Python, using Web Sockets. I wrote client site using HTML5, SASS and JavaScript. You can send text messages and images both from file and url using this chat. Many people can chat at the same time, you only enter using your name.  

## Installing 
To install this app, clone this repo, go to root folder and run:
`npm install`.
When npm finish installing dependencies, just run
`npx http-server`
to start live-reload server locally.