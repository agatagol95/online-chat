var socket = new WebSocket('ws://burek.it:8000');
var displayBox = document.querySelector(".messages");
var text = document.querySelector(".textarea");
var image = document.querySelector(".loadImage");
var sendBtn = document.querySelector(".sendButton");
var enterBtn = document.querySelector(".enter");
var message = {
    type: "",
    text: `dupa`,
    id: '',
    date: Date.now(),
    image: "",
}
socket.onopen = function(event) {
    console.log(event);
    document.querySelector(".errorMsg").classList.add("none");
}
socket.onmessage = function(event) {
    console.log(event.data);
    let response = JSON.parse(event.data);
    let displayResp = document.createElement("div");
    if(response.type == "message") { 
        displayResp.innerHTML = `${response.text}`;
    } else if(response.type == "image") {
        let image = document.createElement("img");
        image.src = response.image;
        displayResp.appendChild(image);
    }
    let id = document.createElement("div");
    id.innerHTML = `${response.id}`;
    let msgData = document.createElement("div");
    msgData.appendChild(id);
    displayResp.appendChild(msgData);
    let displayMsg = document.createElement("div");
    displayMsg.classList.add("displayMsg");
    displayMsg.appendChild(msgData);
    displayMsg.appendChild(displayResp);
    displayBox.appendChild(displayMsg);
    while(displayBox.children.length>10) {
        displayBox.removeChild(displayBox.childNodes[1]);
    }
    displayBox.scrollTop = displayBox.scrollHeight - displayBox.clientHeight;
}
socket.onerror = function() {
    console.log("error occured");
    document.querySelector(".errorMsg").classList.remove("none");
}
function sendMessage() {
    if(text.value && !text.value.includes("https")) {
        message.type="message";
        message.text=text.value;
        socket.send(JSON.stringify(message));
        text.value="";
    } else return "";
}
function sendImage() {
    if(image.files[0]) {
        message.type="image";
        let reader = new FileReader();
        reader.readAsDataURL(image.files[0]);
        reader.onload = function() {
            let imgValue = reader.result;
            message.image=imgValue;
            socket.send(JSON.stringify(message));
            console.log(message);
        }
        image.value="";
    } else if(text.value.includes("https")) {
        message.type="image";
        message.image=text.value;
        socket.send(JSON.stringify(message));
        console.log(message);
        text.value="";
    } else return "";
}
sendBtn.addEventListener("click", e => {
    sendMessage();
    sendImage();
    let loadedFile = document.querySelector(".loadedFile");
    loadedFile.innerHTML = "No file chosen";
});
text.addEventListener("keyup", e=> {
    if(event.keyCode === 13) {
        event.preventDefault();
        sendBtn.click();
    }
})
image.addEventListener("keyup", e=> {
    if(event.keyCode === 13) {
        event.preventDefault();
        sendBtn.click();
    }
});
image.addEventListener("input", e=>{
    let loadedFile = document.querySelector(".loadedFile");
    loadedFile.innerHTML = `${image.value}`;
})
enterBtn.addEventListener("click", e=> {
    let userName = document.querySelector(".userName");
    if(!userName.value=='') {
        message.id = userName.value;
        if(window.width>800) {
            let enterWindow = document.querySelector(".registerUser");
            enterWindow.classList.add("registerUser-trans");
            let chatArea = document.querySelector(".chatArea");
            chatArea.classList.add("chatArea-trans");
        } else {
            let chatWindow = document.querySelector(".chatWindow");
            chatWindow.classList.add("chatWindow-trans");
            let enterWindow = document.querySelector(".registerUser");
            enterWindow.classList.add("registerUser-trans");
            let chatArea = document.querySelector(".chatArea");
            chatArea.classList.add("chatArea-trans");
        };
    } else {
        let errorNote = document.querySelector(".errorNote");
        errorNote.innerHTML = 'Please, enter your name';
        let userName = document.querySelector(".userName");
        userName.classList.add("userName-shake");
    }
})
